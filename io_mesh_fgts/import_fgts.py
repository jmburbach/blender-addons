# This file is part of fg-scenery-tools <gitorious.org/fg-scenery-tools>
#
# Copyright (C) 2010 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License, version 3, as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import bpy

def read(filepath):
	attributes = {}
	fin = open(filepath)
	line = fin.readline().strip()
	assert line == "ATTRIBUTES {"

	line = fin.readline().strip()
	while line != "}":
		name, value = line.split("=")
		attributes[name] = value
		line = fin.readline().strip()

	min_lat = float(attributes["min_lat"])
	max_lat = float(attributes["max_lat"])
	min_lon = float(attributes["min_lon"])
	max_lon = float(attributes["max_lon"])
	median_lat = float(attributes["median_lat"])
	median_lon = float(attributes["median_lon"])
	median_alt = float(attributes["median_alt"])
	span_lat_m = float(attributes["span_lat_m"])
	span_lon_m = float(attributes["span_lon_m"])
	xsamples = int(attributes["xsamples"])
	ysamples = int(attributes["ysamples"])
	offset_lat = -(span_lat_m * 0.5)
	offset_lon = -(span_lon_m * 0.5)
	data = []
	verts = []
	faces = []

	line = fin.readline()
	while line:
		line = line.strip()
		row = map(float, line.split(","))
		data.extend(row)
		line = fin.readline()

	for y in range(ysamples):
		for x in range(xsamples):
			i = 3 * ((y * xsamples) + x)
			lat_m = data[i]
			lon_m = data[i+1]
			alt_m = data[i+2]
			verts.append((
				offset_lon + lon_m,
				offset_lat + lat_m,
				alt_m - median_alt
			))

	data = None

	for y in range(ysamples - 1):
		for x in range(xsamples - 1):
			i = (y * xsamples) + x
			faces.append((i + xsamples, i + xsamples + 1, i + 1, i))

	
	name = bpy.path.display_name_from_filepath(filepath)
	me = bpy.data.meshes.new(name)
	me.from_pydata(verts, [], faces)
	me.update()

	ob = bpy.data.objects.new(name, me)
	ob.data = me
	
	bpy.context.scene.objects.link(ob)
	
	# add some properties to object
	ob["min_lat"] = min_lat
	ob["max_lat"] = max_lat
	ob["min_lon"] = min_lon
	ob["max_lon"] = max_lon
	ob["median_lat"] = median_lat
	ob["median_lon"] = median_lon
	ob["median_alt"] = median_alt
	ob["_RNA_UI"] = { k: { "min": -100000, "max": 100000 } for k in ob.keys() }

