# This file is part of fg-scenery-tools <gitorious.org/fg-scenery-tools>
#
# Copyright (C) 2010 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License, version 3, as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
bl_info = {
	"name": "FlightGear Terrain Sample (.fgts)",
	"description": "Import FlightGear Terrain Sample (.fgts)",
	"author": "Jacob Burbach <jmburbach@gmail.com>",
	"version": (0, 1),
	"blender": (2, 5, 9),
	"location": "File > Import-Export > FlightGear Terrain Sample (.fgts)",
	"category": "Import-Export",
	"license": "GPLv3",
	"link": "http://gitorious.org/fg-scenery-tools",
}

if "bpy" in locals():
	import imp
	if "import_fgts" in locals():
		imp.reload(import_fgts)

import bpy
from bpy.props import StringProperty


class ImportFGTS(bpy.types.Operator):
	bl_idname = "import_mesh.fgts"
	bl_label = "Import FGTS"
	bl_options = {"UNDO"}
	filepath = StringProperty(subtype = "FILE_PATH")
	filter_glob = StringProperty(default = "*.fgts", options = {"HIDDEN"})

	def execute(self, context):
		from . import import_fgts
		import_fgts.read(self.filepath)
		return {"FINISHED"}

	def invoke(self, context, event):
		wm = context.window_manager
		wm.fileselect_add(self)
		return {"RUNNING_MODAL"}


def menu_func(self, context):
	self.layout.operator(ImportFGTS.bl_idname, text = "FlightGear Terrain Sample (.fgts)")

def register():
	bpy.utils.register_module(__name__)
	bpy.types.INFO_MT_file_import.append(menu_func)

def unregister():
	bpy.utils.unregister_module(__name__)
	bpy.types.INFO_MT_file_import.remove(menu_func)

if __name__ == "__main__":
	register()	
