# This file is part of fg-scenery-tools <gitorious.org/fg-scenery-tools>
#
# Copyright (C) 2010 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License, version 3, as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import bpy, os, sys


def export(export_filepath, append = False):
	export_dirpath = os.path.dirname(export_filepath)

	try:
		parent = bpy.context.selected_objects[0]
	except IndexError:
		print("error: Please select a parent object for export")
		return False
	
	median_lon = parent.get("median_lon")
	median_lat = parent.get("median_lat")
	median_alt = parent.get("median_alt")
	if None in (median_lon, median_lat, median_alt):
		print("error: Parent object '{0}' missing one or more required properties.".format(parent.name))
		print("error: Required properties are 'median_lon', 'median_lat', and 'median_alt'")
		print("error: Cannot continue.")
		return False

	children = [ob for ob in parent.children if ob.get("export_flag")]
	if not children:
		print("info: Parent object '{0}' has no children marked for export.".format(parent.name))
		print("info: Nothing to be done")
		return True

	parent.select = False
	parent_saved_loc = list(parent.location)
	parent.location = (0, 0, 0)
	
	try:
		mode = append and "a" or "w"
		stg_file = open(export_filepath, mode)
	except IOError as e:
		print("error: Failed to open '{0}' for writing.".format(export_filepath))
		print("error: {0!r}".format(e))
		return False

	for child in children:
		if not child.get("export_flag"):
			print("info: skipping '{0}', export flag not set".format(child.name))
			continue

		if not child.type == "MESH":
			print("info: skipping '{0}', it is not a mesh".format(child.name))
			continue

		ac_filename = child.get("ac_filename")
		if ac_filename:
			ac_filename = ac_filename.format(
				OBJECT = child.name
			)
		else:
			ac_filename = child.name

		if not ac_filename.endswith(".ac"):
			ac_filename += ".ac"

		ac_abspath = os.path.join(export_dirpath, ac_filename)

		stg_filename = child.get("stg_filename")
		if stg_filename:
			stg_filename = stg_filename.format(
				OBJECT = child.name,
				AC_FILENAME = ac_filename,
			)
		else:
			stg_filename = ac_filename
		
		print("info: exporting '{0}' to '{1}'".format(child.name, ac_abspath))

		child.select = True
		bpy.ops.export_scene.export_ac3d("EXEC_DEFAULT",
			filepath = ac_abspath, use_selection = True)
		child.select = False

		print("info: writing entry for '{0}', as '{1}', to '{2}'".format(
			child.name, stg_filename, export_filepath)
		)

		stg_file.write("OBJECT_STATIC {0} {1} {2} {3} {4}\n".format(
			stg_filename, median_lon, median_lat, median_alt, 90.0)
		)

	parent.select = True
	parent.location = parent_saved_loc
	stg_file.close()

	return True
