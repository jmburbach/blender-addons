# This file is part of fg-scenery-tools <gitorious.org/fg-scenery-tools>
#
# Copyright (C) 2010 Jacob Burbach <jmburbach@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License, version 3, as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
bl_info = {
	"name": "FlightGear Scene (.stg / .ac)",
	"description": "Export scene to FlightGear (.stg / .ac)",
	"author": "Jacob Burbach <jmburbach@gmail.com>",
	"version": (0, 1),
	"blender": (2, 6, 0),
	"location": "File > Import-Export > FlightGear Scene (.stg / .ac)",
	"category": "Import-Export",
	"license": "GPLv3",
	"link": "http://gitorious.org/fg-scenery-tools",
}

if "bpy" in locals():
	import imp
	if "export_scene_fgfs" in locals():
		imp.reload(export_scene_fgfs)

import bpy
from . import export_scene_fgfs


class ExportSceneSTG(bpy.types.Operator):
	bl_idname = "export_scene.stg"
	bl_label = "Export STG"

	filename_ext = ".stg"
	filter_glob = bpy.props.StringProperty(
		default = "*.stg",
		options = {"HIDDEN"}
	)
	filepath = bpy.props.StringProperty(
		subtype = "FILE_PATH"
	)
	check_existing = bpy.props.BoolProperty(
		default = True,
		options = {"HIDDEN"}
	)
	append_to_stg = bpy.props.BoolProperty(
		name = "Append to stg",
		description = "If output stg exists, append to it rather than overwriting",
		default = False,
	)

	def execute(self, context):
		filepath = bpy.path.ensure_ext(self.filepath, self.filename_ext)
		print("====== Beginning export to '{0}' ======".format(filepath))
		success = export_scene_fgfs.export(filepath, self.append_to_stg)
		print("====== Export to '{0}' {1} ====== ".format(
			filepath, success and "successful" or "failed")
		)
		if not success:
			self.report({"ERROR"}, "Export failed, see terminal for details")
		else:
			self.report({"INFO"}, "Export successful")
		return {"FINISHED"}

	def invoke(self, context, event):
		if not self.filepath:
			try:
				ob = bpy.context.selected_objects[0]
				self.filepath = ob.name + self.filename_ext
			except IndexError:
				self.report({"ERROR"}, "No object selected for export.")
				return {"CANCELLED"}
		wm = context.window_manager
		wm.fileselect_add(self)
		return {"RUNNING_MODAL"}


def menu_func(self, context):
	self.layout.operator(ExportSceneSTG.bl_idname, text = "FlightGear Scene (.stg / .ac)")

def register():
	bpy.utils.register_module(__name__)
	bpy.types.INFO_MT_file_export.append(menu_func)

def unregister():
	bpy.utils.unregister_module(__name__)
	bpy.types.INFO_MT_file_export.remove(menu_func)

if __name__ == "__main__":
	register()	
